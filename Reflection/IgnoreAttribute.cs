﻿using System;

namespace Reflection
{
  /// <summary>
  /// A class for identifying whether or not to ignore a type, property or method.
  /// </summary>
  /// <seealso cref="System.Attribute" />
  public sealed class  IgnoreAttribute : Attribute
  {
    #region Fields

    private string _Reason;

    #endregion

    #region Properties

    public string Reason
    {
      get { return _Reason; }
      set { _Reason = value; }
    }

    #endregion

    #region Constructors

    public IgnoreAttribute() {}

    public IgnoreAttribute(string reason)
    {
      _Reason = reason;
    }

    #endregion
  }
}
