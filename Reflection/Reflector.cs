﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Reflection
{
  /// <summary>
  /// A static class that defines utility reflection methods that simplify certain reflection operations.
  /// </summary>
  public static class Reflector
  {
    #region Public Methods

    /// <summary>
    /// Attempts to get the first instance of TAttribute found on the passed in Type.
    /// </summary>
    /// <typeparam name="TAttribute">The type of the attribute we are trying to find.</typeparam>
    /// <param name="type">The type we are trying to find the attribute on.</param>
    /// <returns>The first instance of TAttribute found on type.</returns>
    public static TAttribute GetAttribute<TAttribute>(this Type type)
      where TAttribute : Attribute
    {
      Attribute attribute = Attribute.GetCustomAttribute(type, typeof (TAttribute));

      if (!Equals(attribute, default(TAttribute)))
      {
        return (TAttribute)attribute;
      }

      return default(TAttribute);
    }

    /// <summary>
    /// Checks whether or not the provided type has an attribute of type TAttribute defined.
    /// </summary>
    /// <typeparam name="TAttribute">The type of the attribute we are trying to find.</typeparam>
    /// <param name="type">The type we are trying to find the attribute on.</param>
    /// <returns>True if the type has the attribute otherwise false.</returns>
    public static bool HasAttribute<TAttribute>(this Type type)
      where TAttribute : Attribute
    {
      Attribute attribute = Attribute.GetCustomAttribute(type, typeof(TAttribute));

      return !Equals(attribute, default(TAttribute));
    }

    /// <summary>
    /// Attempts to get the first instance of TAttribute found on the passed in object.
    /// </summary>
    /// <typeparam name="TAttribute">The type of the attribute we are trying to find.</typeparam>
    /// <param name="obj">The object instance we are trying to find the attribute on.</param>
    /// <returns>The first instance of TAttribute found on obj.</returns>
    public static TAttribute GetAttribute<TAttribute>(this object obj)
      where TAttribute : Attribute
    {
      Type type = obj.GetType();
      MemberInfo[] infos = type.GetMember(obj.ToString());

      if (!infos.Any())
      {
        return default(TAttribute);
      }

      object[] attributes = infos[0].GetCustomAttributes(typeof (TAttribute), false);

      if (attributes.Length > 0)
      {
        return (TAttribute) attributes[0];
      }

      return default(TAttribute);
    }

    /// <summary>
    /// Attempts to get the first instance of TAttribute found on the passed in object.
    /// </summary>
    /// <typeparam name="TAttribute">The type of the attribute we are trying to find.</typeparam>
    /// <param name="obj">The object instance we are trying to find the attribute on.</param>
    /// <returns>True if obj has the attribute otherwise false.</returns>
    public static bool HasAttribute<TAttribute>(this object obj)
      where TAttribute : Attribute
    {
      return !Equals(GetAttribute<TAttribute>(obj), default(TAttribute));
    }

    /// <summary>
    /// Finds the first type with a name that matches typeName in the current AppDomian.
    /// </summary>
    /// <param name="typeName">Full name of the type we are looking for.</param>
    /// <returns>A Type instace for the provided typeName if found, otherwise null.</returns>
    public static Type FindType(string typeName)
    {
      return AppDomain.CurrentDomain.GetAssemblies()
        .SelectMany(a => a.GetTypes())
        .FirstOrDefault(t => !t.ShouldBeIgnored() && t.FullName == typeName);
    }

    /// <summary>
    /// Finds the first type with a name that matches typeName in the provided assembly.
    /// </summary>
    /// <param name="assembly">The assembly to look for the type in.</param>
    /// <param name="typeName">Full name of the type we are looking for.</param>
    /// <param name="baseType">
    /// The type of the base class or interface typeName extends.
    /// </param>
    /// <returns>A Type instance for the provided typeName if found. 
    /// If baseType is provided and type found does not implement or extend it null will be returned.</returns>
    /// <exception cref="Exception">
    /// </exception>
    public static Type FindType(Assembly assembly, string typeName, Type baseType = null)
    {
      Type type = assembly.GetType(typeName);

      if (type.ShouldBeIgnored())
      {
        return null;
      }

      if (baseType == null)
      {
        return type;
      }

      if (!baseType.IsInterface)
      {
        return !type.IsSubclassOf(baseType) ? null : type;
      }

      if (!type.GetInterfaces().Contains(baseType))
      {
        return null;
      }

      return !type.IsSubclassOf(baseType) ? null : type;
    }

    /// <summary>
    /// To be called when the resolution of an assembly fails. Must be bound to the AppDomain.AssemblyResolve event.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="args">The <see cref="ResolveEventArgs"/> instance containing the event data.</param>
    /// <returns></returns>
    public static Assembly ResolveAssembly(object sender, ResolveEventArgs args)
    {
      Assembly assembly = Assembly.GetEntryAssembly();

      string resourceName = $"{assembly?.EntryPoint?.DeclaringType?.Namespace ?? "AssemblyLoadingAndReflection."}." +
                            $"{new AssemblyName(args.Name).Name}.dll";

      using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
      {
        if (stream == null || stream == Stream.Null)
        {
          return null;
        }

        byte[] assemblyData = new byte[stream.Length];

        stream.Read(assemblyData, 0, assemblyData.Length);

        return Assembly.Load(assemblyData);
      }
    }

    /// <summary>
    /// Determines whether this assembly is the specified version.
    /// </summary>
    /// <param name="assembly">The assembly that we want to check the version for.</param>
    /// <param name="version">The version to check against the assembly's version.</param>
    /// <returns>True if the version for this assembly matches the version argument otherwise false.</returns>
    public static bool IsThisVersion(this Assembly assembly, string version)
    {
      AssemblyName assemblyName = assembly.GetName();

      return assemblyName.Version.ToString().Equals(version);
    }

    /// <summary>
    /// Invokes the specified method on the provided object using any paramaters that have also provided.
    /// </summary>
    /// <param name="methodInfo">The method we want to invoke.</param>
    /// <param name="toInvokeOn">To object we want to invoke the method on.</param>
    /// <param name="args">The arguments we want to pass into the method.</param>
    /// <returns>The return value of the method to call.</returns>
    public static object Invoke(this MethodInfo methodInfo, object toInvokeOn, params object[] args)
    {
      return methodInfo.Invoke(toInvokeOn, args);
    }

    /// <summary>
    /// Attempts to find the Type for the provided type and instantiate it. 
    /// Generic method so user does not have to worry about casting.
    /// </summary>
    /// <typeparam name="T">The type we are going to return the instantiated type as.</typeparam>
    /// <param name="typeName">Full name of the type to instantiate.</param>
    /// <param name="assembly">
    /// The assembly to look for the type in. If not provided all we will look in the current AppDomain.
    /// </param>
    /// <param name="args">An array of arguments to pass into a constructor.</param>
    /// <returns>An instance of typeName if found, otherwise the default value of T.</returns>
    public static T Instantiate<T>(string typeName, Assembly assembly = null, params object[] args)
    {
      if (assembly == null)
      {
        assembly = typeof (T).Assembly;
      }

      Type classType = typeof(T).FullName != typeName 
        ? FindType(assembly, typeName, typeof (T)) 
        : FindType(assembly, typeName);
      
      if (classType == null)
      {
        return default(T);
      }

      if (args == null || args.Length == 0)
      {
        return (T) Activator.CreateInstance(classType);
      }

      return (T) Activator.CreateInstance(classType, new object[] { args });
    }

    /// <summary>
    /// Creates in instance of type T.
    /// </summary>
    /// <typeparam name="T">The type to create an instance of.</typeparam>
    /// <param name="args">An array of arguments to pass into a constructor.</param>
    /// <returns>An instance of T.</returns>
    public static T Instantiate<T>(params object[] args)
    {
      if (args == null || args.Length == 0)
      {
        return (T) Activator.CreateInstance(typeof (T));
      }

      return (T) Activator.CreateInstance(typeof (T), new object[] {args});
    }

    /// <summary>
    /// Checks if the passed in object has been marked down to be ignored.
    /// </summary>
    /// <param name="obj">The object to inspect.</param>
    /// <returns>True if IgnoreAttribute is found on obj otherwise false.</returns>
    public static bool ShouldBeIgnored(this object obj)
    {
      return obj.HasAttribute<IgnoreAttribute>();
    }

    /// <summary>
    /// Checks if the passed in Type has been marked down to be ignored.
    /// </summary>
    /// <param name="type">The type to inspect.</param>
    /// <returns>True if IgnoreAttribute is found on type otherwise false.</returns>
    public static bool ShouldBeIgnored(this Type type)
    {
      return type.HasAttribute<IgnoreAttribute>();
    }

    #endregion
  }
}