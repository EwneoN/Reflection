﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Reflection
{
  /// <summary>
  /// A class for the management and resolution of assemblies that have been loaded into memory at runtime.
  /// </summary>
  public static class DynamicallyLoadedAssemblies
	{
    #region Fields

    /// <summary>
    /// A thread safe dictionary for the storage on the dynamically loaded assemblies.
    /// </summary>
    private static readonly ConcurrentDictionary<string, Assembly> _DynamicallyLoadedAssemblies;

    #endregion

    #region Constructors

    static DynamicallyLoadedAssemblies()
		{
			_DynamicallyLoadedAssemblies = new ConcurrentDictionary<string, Assembly>();

			AppDomain.CurrentDomain.AssemblyResolve += ResolveLibAssemblyHandler;
		}

    #endregion

    #region Public Methods

    /// <summary>
    /// Adds the provided assembly to the collection.
    /// </summary>
    /// <param name="assembly">The assembly to add.</param>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException">Assembly has already been added</exception>
    /// <exception cref="InvalidOperationException">Failed to add assembly</exception>
    public static void Add(Assembly assembly)
		{
			if (assembly == null)
			{
				throw new ArgumentNullException(nameof(assembly));
			}

			if (_DynamicallyLoadedAssemblies.ContainsKey(assembly.FullName))
			{
				throw new ArgumentException("Assembly has already been added");
			}

			if (!_DynamicallyLoadedAssemblies.TryAdd(assembly.FullName, assembly))
			{
				throw new InvalidOperationException("Failed to add assembly");
			}
		}

    /// <summary>
    /// Tries to add the provided assembly to the collection.
    /// </summary>
    /// <param name="assembly">The assembly to add.</param>
    /// <returns>True if the item was added otherwise false.</returns>
    public static bool TryAdd(Assembly assembly)
    {
      return assembly != null && _DynamicallyLoadedAssemblies.TryAdd(assembly.FullName, assembly);
    }

    /// <summary>
    /// Removes the specified assembly from the collection if it can be found.
    /// </summary>
    /// <param name="assemblyFullName">Full name of the assembly to remove.</param>
    /// <exception cref="ArgumentException">Cannot be null, empty or whitespace</exception>
    /// <exception cref="KeyNotFoundException">Assembly could not be found for + assemblyFullName</exception>
    /// <exception cref="InvalidOperationException">Failed to remove assembly</exception>
    /// <returns>The specified assembly if the get operation was successful otherwise null.</returns>
    public static Assembly Remove(string assemblyFullName)
		{
			if (string.IsNullOrWhiteSpace(assemblyFullName))
			{
				throw new ArgumentException("Cannot be null, empty or whitespace", nameof(assemblyFullName));
			}

      // we check this rather than just relying on TryRemove as it could fail for other reasons. This allows more info.
      if (!_DynamicallyLoadedAssemblies.ContainsKey(assemblyFullName))
      {
        throw new KeyNotFoundException("Assembly could not be found for " + assemblyFullName);
      }

      Assembly assembly;

			if (!_DynamicallyLoadedAssemblies.TryRemove(assemblyFullName, out assembly))
			{
				throw new InvalidOperationException("Failed to remove assembly");
			}

      return assembly;
		}

    /// <summary>
    /// Tries to remove the specified assembly from the collection if it can be found.
    /// </summary>
    /// <param name="assemblyFullName">Full name of the assembly to remove.</param>
    /// <param name="assembly">The specified assembly if the remove operation was successful.</param>
    /// <returns>True if the assembly was removed otherwise false.</returns>
    public static bool TryRemove(string assemblyFullName, out Assembly assembly)
    {
      assembly = null;

      return !string.IsNullOrWhiteSpace(assemblyFullName) && 
             _DynamicallyLoadedAssemblies.TryRemove(assemblyFullName, out assembly);
    }

    /// <summary>
    /// Retrieves an assembly for the provided assembly full name if it can be found in this collection.
    /// </summary>
    /// <param name="assemblyFullName">Full name of the assembly to retrieve.</param>
    /// <returns>An assemly with a full name that matches assemlyFullName</returns>
    /// <exception cref="ArgumentException">Cannot be null, empty or whitespace</exception>
    /// <exception cref="KeyNotFoundException">Assembly could not be found for + assemblyFullName</exception>
    /// <exception cref="InvalidOperationException">Failed to get assembly</exception>
    public static Assembly Get(string assemblyFullName)
		{
			if (string.IsNullOrWhiteSpace(assemblyFullName))
			{
				throw new ArgumentException("Cannot be null, empty or whitespace", nameof(assemblyFullName));
			}

      // we check this rather than just relying on TryGetValue as it could fail for other reasons. This allows more info.
      if (!_DynamicallyLoadedAssemblies.ContainsKey(assemblyFullName))
			{
				throw new KeyNotFoundException("Assembly could not be found for " + assemblyFullName);
			}

			Assembly assembly;

			if (!_DynamicallyLoadedAssemblies.TryGetValue(assemblyFullName, out assembly))
			{
				throw new InvalidOperationException("Failed to get assembly");
			}

			return assembly;
    }

    /// <summary>
    /// Retrieves an assembly for the provided assembly full name if it can be found in this collection.
    /// </summary>
    /// <param name="assemblyFullName">Full name of the assembly to retrieve.</param>
    /// <param name="assembly">The specified assembly if the get operation was successful.</param>
    /// <returns>An assemly with a full name that matches assemlyFullName</returns>
    public static bool TryGet(string assemblyFullName, out Assembly assembly)
    {
      assembly = null;

      if (string.IsNullOrWhiteSpace(assemblyFullName))
      {
        return false;
      }

      return !_DynamicallyLoadedAssemblies.TryGetValue(assemblyFullName, out assembly);
    }

    /// <summary>
    /// Clears this collection and resets the count to 0.
    /// </summary>
    public static void Clear()
		{
			_DynamicallyLoadedAssemblies.Clear();
		}

    #endregion

    #region Event Handlers

    /// <summary>
    /// <para>
    /// Wired up to be called when the resolution of an assembly fails. This will look in the assembly collection to see
    /// if it has been loaded at runtime. If it cannot find the assembly in the collection it will then look for the assembly
    /// in the asseblies already loaded into the current AppDomain.
    /// </para>
    /// <para>
    /// This event handler allows us to store binaries that are loaded into the environment at runtime
    /// in locations other than the application root or it's sub directories. Especially useful for loading the dependencies
    /// of assemblies that have been loaded at run time. 
    /// </para>
    /// </summary>
    /// <param name="sender">The object that has raised the event.</param>
    /// <param name="args">The <see cref="ResolveEventArgs"/> instance containing the assembly resolution data.</param>
    /// <returns>The resloved assembly if it could be found otherwise null.</returns>
    private static Assembly ResolveLibAssemblyHandler(object sender, ResolveEventArgs args)
		{
			Assembly assembly;

			if (_DynamicallyLoadedAssemblies.TryGetValue(args.Name, out assembly))
			{
				return assembly;
			}

			if (!args.Name.Contains("Retargetable=Yes"))
			{
				return AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName.Equals(args.Name));
			}

			Regex regex = new Regex(@"^[a-zA-Z0-9\.]+(?=,)");
			Match match = regex.Match(args.Name);
			string name = match.Value;

			return AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.GetName().Name.Equals(name));
		}

		#endregion
	}
}
