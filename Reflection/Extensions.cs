﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Reflection
{
	public static class Extensions
	{
    #region Public Methods

    /// <summary>
    /// Determines whether the provided property is an array type.
    /// </summary>
    /// <param name="property">The property to inspect.</param>
    /// <returns>True if the property type is an array type otherwise false.</returns>
    public static bool IsPropertyAnArray(this PropertyInfo property)
		{
			return property.PropertyType.IsArray;

		}

    /// <summary>
    /// Determines whether the provided property is an collection type.
    /// </summary>
    /// <param name="property">The property to inspect.</param>
    /// <returns>True if the property type is a collection type otherwise false.</returns>
    public static bool IsPropertyACollection(this PropertyInfo property)
		{
			return property.PropertyType.IsTypeACollection();

		}

    /// <summary>
    /// Determines whether the type is a collection. Does this by checking if it is an array or 
    /// if it implements IEnumerable or IEnumerable&lt;&gt;.
    /// </summary>
    /// <param name="type">The type to inspect.</param>
    /// <returns>True if the type is a collection type otherwise false.</returns>
    public static bool IsTypeACollection(this Type type)
		{
			return type.GetInterface(typeof (IEnumerable).FullName) != null ||
						 type.GetInterface(typeof (IEnumerable<>).FullName) != null ||
						 type.IsArray;
		}

		#endregion
	}
}
